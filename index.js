const { Socket } = require('dgram');
const express = require('express');
const app = express();
require('dotenv').config();
const path = require('path');

const serve = require('http').createServer(app);
module.exports.io = require('socket.io')(serve);
require('./sockets/socket');

//path publico
const publicPtah = path.resolve(__dirname, 'public');
app.use(express.static(publicPtah));

serve.listen(process.env.PORT, (err) => {
    if (err) throw new Error(err);
    console.log('Servidor corriendo en el puerto', process.env.PORT);
});