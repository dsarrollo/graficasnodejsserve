const { io } = require('../index');
io.on('connection', client => {
    console.log('Cliente se conecto');
    client.on('disconnect', () => {
        console.log('Cliente se desconecto');
        //client.emit('logout', { admin: 'message new' });
    });

    client.on('newMessage', (data) => {
        //io.emit('newMessage', { admin: 'message new' }); // emite a todos los usuarios que estan conectados al socket
        client.broadcast.emit('newMessage', data); // emite a todos menos al que envio el mensaje
    });
});